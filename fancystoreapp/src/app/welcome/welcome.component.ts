import { Component, OnInit, ViewEncapsulation, VERSION, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SideNavComponent  } from '../side-nav/side-nav.component';
import { NavService } from '../services/nav.service';
import { NavItem } from '../models/nav-item';

import { Router } from '@angular/router';
import { CommonService } from '../services/common.service';


import { EntryService } from '../services/entry.service';
import { HttpErrorResponse } from '@angular/common/http';


@Component({

  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
  encapsulation: ViewEncapsulation.None
})


export class WelcomeComponent implements AfterViewInit {
  @ViewChild('appDrawer') appDrawer: ElementRef;
  version = VERSION;

  navItems: NavItem[];

  readonly googlePlayLink: string;
  readonly appStoreLink: string;

 

  accid:string;
  items: Array<any>;
  products: string[];
 prodcategories:string[]; 
 prodsubcategories:string[];
flag:boolean=false;
public show:boolean=false;

  constructor(private navService: NavService, private entryService: EntryService, private router: Router) {
    this.navItems = this.navService.navItems;

    this.entryService.getproducts().subscribe(
      data=>{
        this.products=data as string[];
    
    },
    (err: HttpErrorResponse) => {
      console.log (err.message);
      
    }
    );
  }

  ngAfterViewInit() {
    this.navService.appDrawer = this.appDrawer;
  }





  productaccord(id: string, category: string){
    console.log('accordian id::'+id);
    debugger
    if(category === 'main'&& this.flag==false){
      debugger
     this.entryService.getpcategories(id).subscribe(
      data=>{
        this.prodcategories=data as string[];
            debugger
     
    },
    (err: HttpErrorResponse) => {
      console.log (err.message);
      
    });
  }

  if(category === 'sub'){
    debugger
    this.flag=true;
    this.entryService.getpscategories(id).subscribe(
    data=>{
     this.prodsubcategories=data as string[];
    
    if(this.prodsubcategories!=null) 
    {
    debugger 
    this.show=true;
  }
  else{
    this.show=false;
  }

  },
  (err: HttpErrorResponse) => {
    console.log (err.message);
    debugger
  });
   return this.flag;
  }
}


}
