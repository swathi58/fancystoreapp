import { Component, ViewChild } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
 
})
export class AppComponent {

  show: boolean = true;

  title = 'fancystoreapp';

  constructor(public dialog: MatDialog, private router: Router, private location: Location) {

    this.router.navigateByUrl("/home");
    //console.log("app call2");
    //this.ngOnInit();
  }


  ngOnInit() {
    //console.log("app call");
    //if (!sessionStorage.getItem("home")) {
     
    //}
    //else {
    //  this.show = false;
    //}
  }
  //load() {
  //  console.log("app call3");
  //  location.reload()
  //}


}

@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: 'dialog-content-example-dialog.html',
})
export class DialogContentExampleDialog { }


