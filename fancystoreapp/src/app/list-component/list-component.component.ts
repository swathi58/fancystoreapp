import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from '../services/admin.service';
import { Vendor } from '../models/Vendor';
import { ActivatedRoute, Router } from '@angular/router';
import { UserPersonalData } from '../models/UserPersonalData';
import { Address } from '../models/AddressModel';

@Component({
  selector: 'app-list-component',
  templateUrl: './list-component.component.html',
  styleUrls: ['./list-component.component.scss']
})
export class ListComponentComponent implements OnInit {
 
Role:string;
Ltype:string;

vendor: Vendor;
user:UserPersonalData;
adr:Address;

vendorList:Array<any> = [];


  constructor(private adminService: AdminService,private toastr : ToastrService,private route:ActivatedRoute,private router: Router,) {     
  }

  ngOnInit() {
     //This is Check Vendor / User collection
     this.Role = this.route.snapshot.params['Role'];  
    //This is Check Type of List
    this.Ltype=this.route.snapshot.params['LType']; 
    this.showForEdit();   
  }


  showForEdit() 
  {
    debugger;
    if (this.Ltype=="AlV" || this.Ltype=="AcV" || this.Ltype=="DacV")
    {      
      //this.adminService.selectedVendor = Object.assign({}, this.vendor);
      this.adminService.GetVendorList().subscribe((vdata: Vendor[])=>{
        this.vendorList = vdata["vendordata"]["table"];
        //console.log(this.vendorList);
      },
      error => {
        if (error["status"] == 401) {         
          this.toastr.error("Your session expired");
          sessionStorage.clear();
          this.router.navigate(['/vendor']);
        }

      });  
      
    }
    else
    {
      this.adminService.selectedUser = Object.assign({}, this.user);;
    }
   
  }
 
 
  onDelete(id: number) {
    // if (confirm('Are you sure to delete this record ?') == true) {
    //   this.adminService.deleteVendor(id)
    //   .subscribe(x => {
    //     this.adminService.getVendorList();
    //     this.toastr.warning("Deleted Successfully","Vendor Register");
    //   })
    // }
  }
}
