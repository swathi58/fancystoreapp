import { Component, OnInit } from '@angular/core';
import { LoginComponent } from '../login/login.component';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NavItem, SmsfashionapiService } from '../services/smsfashionapi.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loginsts: boolean = false;
  constructor(public dialog: MatDialog, private router: Router, private toastr: ToastrService, private dataservice: SmsfashionapiService ) {

  }

  ngOnInit() {

  
  //test
  //eswar
    sessionStorage.setItem("home", "true");

  }

  loginmodel() {
  //  console.log("dsfhsdh");

    if (sessionStorage.getItem("token")) {
      this.router.navigateByUrl('/home');
      this.loginsts = true;
      this.toastr.success("Hello   " + sessionStorage.getItem("userName"), "Welcome To Fashion");
      this.BagInfomation();

    }
    else {
      this.loginsts = false;
      this.dialog.open(LoginComponent/*, { width: '550px', height: '600px' }*/);

      //sessionStorage.removeItem("token");
      //sessionStorage.removeItem("userName");
      //sessionStorage.removeItem("uid");
      //sessionStorage.removeItem("email");
      //sessionStorage.removeItem("expiration");
      //this.router.navigate(['/home']);
    }

   
  }

  womenstore() {

    this.router.navigateByUrl('/womenstore');
  }

  vendordata()
  {
    if (!sessionStorage.getItem("token"))
    {
      this.router.navigateByUrl('/vendor');
    }
    else{
      this.router.navigateByUrl('/welcome');
    }
    console.log(sessionStorage.getItem("UType"))
  }


  BagInfomation() {
    this.dataservice.GetBagItemsDetailsById(Number.parseInt(sessionStorage.getItem("uid"))).subscribe(x => {
      this.dataservice.bagiformationbyid = x["res"]["table"];
    });
  }
}
