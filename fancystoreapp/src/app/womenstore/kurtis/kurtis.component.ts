import { Component, OnInit, ViewChild, ElementRef, VERSION } from '@angular/core';
import { SmsfashionapiService, NavItem } from '../../services/smsfashionapi.service';
import { Router } from '@angular/router';
import { LoginComponent } from '../../login/login.component';
import { MatDialog, MatMenuTrigger } from '@angular/material';
import { PlatformLocation } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-kurtis',
  templateUrl: './kurtis.component.html',
  styleUrls: ['./kurtis.component.scss']
})
export class KurtisComponent implements OnInit {
  @ViewChild('appDrawer') appDrawer: ElementRef;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  version = VERSION;
  show: boolean = true;
  navItems: NavItem[];
  items: Array<any> = [];
  catcount: Number;
  applog: string;
  name: string = "kurtis";


  constructor(private dataservice: SmsfashionapiService, private router: Router, public dialog: MatDialog, private platformLocation: PlatformLocation,
    private toastr: ToastrService) {
    this.applog = this.dataservice.getapplog();
    //console.log(this.platformLocation.pathname);

    this.dataservice.GetItemsdata(this.name).subscribe(x => {
      console.log(x["res"]["table"]);
      this.items = x["res"]["table"];
    });

    //this.items = [
    //  { img:'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/Kurtis/kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 }
    //]
    this.navItems = this.dataservice.navItems;

    //if (sessionStorage.getItem("catcount")) {
    //  this.catcount = Number.parseInt(sessionStorage.getItem("catcount"));
    //}
    //else {
    //  this.catcount = 0;
    //}
  
    this.catcount = this.dataservice.GetBagCount();
  }

  ngOnInit() {


  }
  ngAfterViewInit() {
    this.dataservice.appDrawer = this.appDrawer;
  }
  userprofile() {
  }
  openMyMenu() {
    //this.trigger.openMenu();
    if (!sessionStorage.getItem("token")) {
      this.dialog.open(LoginComponent);
    }
    else if (sessionStorage.getItem("token")) {

      this.trigger.openMenu();
    }

  }
  openorderspage() {

    this.router.navigateByUrl("/usersettings/orders");
  }
  prifilepage() {
    this.router.navigateByUrl("/usersettings/editprofile");
  }

  logout() {
    this.router.navigate(['/logout']);
  }

  displayitemdesc(id) {

    this.dataservice.item_id = Number.parseInt(id);
    this.router.navigate(['/itemdesc']);
  }
  viewbag(): void {
    if (this.catcount > 0) {
      this.router.navigateByUrl("/itembag");
    }
    else {
      this.toastr.info("No Itemes In Your Cart", "Bag", { timeOut: 3000, positionClass: 'toast-top-right' });
    }
  }


  whishlist() {
    this.router.navigateByUrl("/whishlist");
  }
}
