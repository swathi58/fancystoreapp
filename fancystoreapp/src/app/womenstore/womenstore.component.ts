import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { ModalDirective } from 'angular-bootstrap-md';
import { SmsfashionapiService } from '../services/smsfashionapi.service';


@Component({
  selector: 'app-womenstore',
  templateUrl: './womenstore.component.html',
  styleUrls: ['./womenstore.component.scss']
})
export class WomenstoreComponent implements OnInit {
  @ViewChild('basicModal') public showModalOnClick: ModalDirective;
  applog: string;
  womenscatgrytypes: Array<any> = [];

  wmnoffers: Array<any> = [];
  brands: Array<any> = [];
  womencatgcatlog: string;;
womencatgcatlog2: string;
  womencatgcatlog3: string;

  catcount: number;
  constructor(private router: Router, private toastr: ToastrService, public dialog: MatDialog, private dataservice: SmsfashionapiService) {


    this.applog = this.dataservice.getapplog();

    this.womenscatgrytypes = this.dataservice.womencatgrytypeimags;

    this.wmnoffers = this.dataservice.offers;

    this.brands = this.dataservice.brands;

    this.womencatgcatlog = "assets/MyImages/Kurtis/kurtistypesicon.jpeg";
    this.womencatgcatlog2 = "assets/MyImages/sarees/catsarees.jpg";
    this.womencatgcatlog3 = "assets/MyImages/tops/topware3.jpeg";
    //this.womenscatgrytypes = [
    //  { img: 'assets / MyImages / Kurtis / kurtistypesicon.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/sarees/catsarees.jpg', name: 'Blue Printed Kurta', price: 1200 },
    //  { img: 'assets/MyImages/tops/topware3.jpeg', name: 'Blue Printed Kurta', price: 1200 },
    // // assets / MyImages / Kurtis / kurtistypesicon.jpeg
    //];


   // this.dataservice.GetMainLogo().subscribe(x => { console.log(x) })


    if (sessionStorage.getItem("uid")) {
     // console.log(this.dataservice.GetBagCount());
      this.catcount = this.dataservice.GetBagCount();
      this.toastr.success("Hello   " + sessionStorage.getItem("userName"), "Welcome To Fashion");
    }
    else {

      sessionStorage.removeItem("token");
      sessionStorage.removeItem("userName");
      sessionStorage.removeItem("uid");
      sessionStorage.removeItem("email");
      sessionStorage.removeItem("expiration");
     
      this.catcount = this.dataservice.GetBagCount();
    }
  

  }




  ngOnInit() {

    }
  
  kurtasList(id) {
    this.dataservice.CatNavigationUrls(id);   
  }
  userprofile() {

    //if (!sessionStorage.getItem("token")) {
    // this.dialog.open(LoginComponent);
    //}
    //else {    
    //}
  }

  viewbag(): void {
    if (this.catcount > 0) {

      this.router.navigateByUrl("/itembag");
    }
    else {
      this.toastr.info("No Itemes In Your Cart", "Bag", { timeOut: 3000, positionClass: 'toast-top-right'});
    }
  }
}


