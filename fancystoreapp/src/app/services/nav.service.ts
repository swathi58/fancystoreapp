import {EventEmitter, Injectable} from '@angular/core';
import {Event, NavigationEnd, Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import{ NavItem} from '../models/nav-item';

@Injectable()
export class NavService {
  public appDrawer: any;
  public currentUrl = new BehaviorSubject<string>(undefined);

  Role: string; 

    constructor(private router: Router) {

      this.Role = sessionStorage.getItem("UType");  

    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl.next(event.urlAfterRedirects);
      }
    });
  
    this.Role = sessionStorage.getItem("UType");  

  }

  public closeNav() {
    this.appDrawer.close();
  }

  public openNav() {
    this.appDrawer.open();
  }

  

  navItems: NavItem[] = [
    {      
      displayName: 'Admin',
      iconName: 'recent_actors',
      route: 'welcome',
      children: [
        { 
          displayName: 'Personal Details',
          iconName: 'edit',//perm_identity
          route: ('Eprofile/' + sessionStorage.getItem("UType")) //this.Role is showing undefine
        }
       ]
    },
    {
      displayName: 'Vendor',
      iconName: 'group',      
      children: [
        {
          displayName: 'List of All Vendors',
          iconName: 'person',//perm_identity
          route: ('List/'+ sessionStorage.getItem("rid") + '/' + 'AlV')
          
        },
        {
          displayName: 'Active Vendors List',
          iconName: 'person',//perm_identity
          route: ('List/'+ sessionStorage.getItem("rid") + '/' + 'AcV')
        },
        {
          displayName: 'DeActive Vendors List',
          iconName: 'person',//perm_identity
          route: ('List/' + sessionStorage.getItem("rid") + '/' + 'DacV')
        }
        ]
    },
    {
      displayName: 'User',
      iconName: 'group',
      children: [
        {
          displayName: 'List of Users',
          iconName: 'person',//perm_identity
          route: ('List/'+ sessionStorage.getItem("rid") + '/' + 'AlU')
        }
      ]
     },
    {
      displayName: 'Product',
      disabled: true,
      iconName: 'add_shopping_cart', //store_mall_directory',
      children: [
        {
          displayName: 'List of Products',
          iconName: 'store_mall_directory',//perm_identity
          route: 'List'
        }
      ]
    },
    {
      displayName: 'Reports',
      disabled: true,
      iconName: 'crop_portrait'//'pages' //picture_as_pdf
    },
    {
    displayName: 'LogOut',
    disabled: true,
    iconName: 'power_settings_new',//'pages' //picture_as_pdf
     route:'logout'
    }
  ];

}