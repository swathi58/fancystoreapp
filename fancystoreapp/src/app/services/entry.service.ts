import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Vendor } from '../models/Vendor';
import { map } from 'rxjs/operators';
import{Response,RequestMethod,Headers,RequestOptions}from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class EntryService {


  readonly rootUrl = 'https://localhost:44307';//'http://10.180.9.156:82';

  //readonly rootUrl = 'https://localhost:44307';//'http://10.180.9.156:82'

  constructor(private http: HttpClient) { }
 
  registerUser(vendor : Vendor){
    const body : Vendor = {
      FirstName: vendor.FirstName,
      LastName:vendor.LastName,
      Email: vendor.Email,
      Password: vendor.Password,
      Gender: vendor.Gender,
      PhoneNo: vendor.PhoneNo,
      ConfirmPassword:"" ,
      Logo:vendor.Logo,
      Image:vendor.Image,
      //VID:vendor.VID,
      PID: vendor.PID,
      //DOR: vendor.DOR,
      ISActive: vendor.ISActive 
    }     
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
    return this.http.post(this.rootUrl + '/api/Entry/Register', body,{headers : reqHeader});
  }


  registerVender( vendor : Vendor){
    const body : Vendor = {
      FirstName: vendor.FirstName,
      LastName:vendor.LastName,
      Email: vendor.Email,
      Password: vendor.Password,
      Gender:vendor.Gender,
      Image:vendor.Image,
      PhoneNo: vendor.PhoneNo,
      ConfirmPassword:vendor.ConfirmPassword,
      Logo: vendor.Logo,
      PID: vendor.PID,
      //DOR: vendor.DOR,
      ISActive: vendor.ISActive
    }  
        
    var reqHeader = new HttpHeaders({'No-Auth':'True'});
    return this.http.post(this.rootUrl + '/api/Entry/Register', body,{headers : reqHeader});
  }

  userAuthentication(Email, Password) {
   // var data = "Email=" + userName + "&password=" + password;  
   var data = "/" + Email +"/" + Password; 
  //  console.log("jSone"+ JSON.stringify(data));
    var reqHeader = new HttpHeaders({ 'Content-Type': 'application/json'});
    // return this.http.post(this.rootUrl + '/api/Entry/login/'+Email+'/'+Password+'', JSON.stringify(data), { headers: reqHeader }); 
    console.log(this.rootUrl + '/api/Entry/AVLogin'+ data +'',data, { headers: reqHeader });   
    return this.http.post(this.rootUrl + '/api/Entry/AVLogin'+ data +'',data, { headers: reqHeader });  
  } 

  insproductvalues(pName){
    debugger
    var reqHeader=new HttpHeaders({'Content-Type':'application/json'});
    return this.http.post(this.rootUrl+'/api/Vendor/insproductname/'+pName+'','',{headers:reqHeader});
  }

  bindproduct(){
    debugger
    this.http.get(this.rootUrl+'/api/Vendor/getproduct') 
    .pipe(map((data:Response)=>{
      console.log(data);      
      var a={};
       a=data;
      debugger
      return data.json();
      
    }))
  }

  getproducts(){
    
    return this.http.get(this.rootUrl+'/api/Vendor/getproduct');
  }

  getpcategories(PID){
    return this.http.get(this.rootUrl+'/api/Vendor/getpcategories/'+PID);
  }


  getpscategories(prod_cat_id){
    return this.http.get(this.rootUrl+'/api/Vendor/getpscategories/'+prod_cat_id);
  }

}
