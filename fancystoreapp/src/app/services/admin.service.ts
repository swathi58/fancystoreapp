import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,} from '@angular/common/http';
//import {  BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { Vendor } from '../models/Vendor';
import { Address } from '../models/AddressModel';
import { UserPersonalData } from '../models/UserPersonalData';



@Injectable({
  providedIn: 'root'
})
export class AdminService {

  selectedVendor: Vendor;
  selectedUser:UserPersonalData;
  selectedadr:Address;

  constructor(private http: HttpClient, private router: Router) {

  }


  readonly rootUrl = 'https://localhost:44307';
  //readonly rootUrl = 'http://10.180.9.156:82'

  GetVendorList(){
    return this.http.get(this.rootUrl + '/api/Vendor/GetAllVendorsList', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + sessionStorage.getItem("token")
      })
    })
   
  }

  DeleteVendor(id: number) {
    return this.http.delete(this.rootUrl + '/api/Vendor/DeleteVendorById/'  + id, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + sessionStorage.getItem("token")
      })
    })
  }



}

