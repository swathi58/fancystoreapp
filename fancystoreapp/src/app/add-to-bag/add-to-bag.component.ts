import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatMenuTrigger } from '@angular/material';
import { Router } from '@angular/router';
import { SmsfashionapiService, NavItem } from '../services/smsfashionapi.service';
import { LoginComponent } from '../login/login.component';
import { Items } from '../models/ItemsModel';

@Component({
  selector: 'app-add-to-bag',
  templateUrl: './add-to-bag.component.html',
  styleUrls: ['./add-to-bag.component.scss']
})
export class AddToBagComponent implements OnInit {
  show: boolean = true;
  navItems: NavItem[];
  BagItems: Array<any>;
  count: number;
  totalprice: number = 0;
  shpngchrgs: number=0;
  totalamount: number = 0;
  localbagitems: Array<any> = [];


  addtobagitems: Items = {
    uid: null,
    itm_id: null,
    it_name: null,
    price: null,
    cat_img: null,
    size: null,
    qunty: null,
    bag_id: null,
    type: null,
   
  }

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  constructor(private toastr: ToastrService, public dialog: MatDialog, private router: Router, private dataservice: SmsfashionapiService) {
    this.navItems = this.dataservice.navItems;


  



    if (!sessionStorage.getItem("uid")) {
      console.log(localStorage.getItem("unknownuserBAG"));
      this.BagItems = JSON.parse(localStorage.getItem("unknownuserBAG"));
      this.count = this.BagItems.length;    
     
    }
    else
    {
      if (localStorage.getItem("unknownuserBAG")) {
        this.BagItems = this.dataservice.bagiformationbyid;
        this.localbagitems = JSON.parse(localStorage.getItem("unknownuserBAG"));

        this.addtobagitems.uid=(Number.parseInt(sessionStorage.getItem("uid")));

        this.localbagitems.forEach((data) => {
          this.BagItems.push(data);
          this.addtobagitems.itm_id = data["itm_id"];
          this.addtobagitems.price = data["price"];
          this.addtobagitems.qunty = data["qunty"];
          this.addtobagitems.size = data["size"];


          this.dataservice.AddingItemsToBag(this.addtobagitems).subscribe(x => {

            //console.log(x);
            //this.toastr.success(x["res"], 'BAG', {
            //  timeOut: 3000, positionClass: 'toast-top-right'
            //});
          });

          this.count = this.BagItems.length;    
        });
        localStorage.removeItem("unknownuserBAG");
        localStorage.removeItem("bagcount");
      }
      else {
        this.BagItems = this.dataservice.bagiformationbyid;
        this.count = this.dataservice.bagiformationbyid.length;
      }
      //else {
      //  console.log(this.dataservice.bagiformationbyid);
      //  this.BagItems = this.dataservice.bagiformationbyid;
      //  this.count = this.dataservice.bagiformationbyid.length;
      //}
      //console.log(this.dataservice.bagiformationbyid);
      //this.BagItems = this.dataservice.bagiformationbyid;
      //this.count = this.dataservice.bagiformationbyid.length;


    }

    this.BagItems.forEach((data) => {
      this.totalprice = this.totalprice + Number.parseInt(data["price"]);
    });
    this.totalamount = this.totalprice + this.shpngchrgs;
    sessionStorage.setItem("totalprice", this.totalprice.toString());
    sessionStorage.setItem("totalamount", this.totalamount.toString());
    sessionStorage.setItem("shpngchrgs", this.shpngchrgs.toString());
  }

  ngOnInit() {
  }
  userprofile() {

    if (!sessionStorage.getItem("token")) {
      this.dialog.open(LoginComponent);
    }
    else {

      this.show = false;
    }
  }
  openMyMenu() {
    //this.trigger.openMenu();
    if (!sessionStorage.getItem("token")) {
      this.dialog.open(LoginComponent);
    }
    else if (sessionStorage.getItem("token")) {

      this.trigger.openMenu();
    }

  }

  movetowishlist(item) {
   // console.log(item);
    this.addtobagitems.uid = Number.parseInt(sessionStorage.getItem("uid"));
    this.addtobagitems.itm_id = item["itm_id"];
    this.addtobagitems.bag_id = item["bag_id"];
    this.addtobagitems.price = item["price"];
    this.addtobagitems.qunty = item["qunty"];
    this.addtobagitems.type = 1; //for move to whish list

    this.dataservice.Comm_Service_Movetowhishlist_Remove(this.addtobagitems).subscribe(x => {
   //   console.log(x)

      if (x["res"] = "Successfull") {
        this.toastr.info("Item Added to Whish List", "WhishList", { timeOut: 3000, positionClass: 'toast-top-right' });
        const index: number = this.BagItems.indexOf(item);
        if (index !== -1) {
          this.BagItems.splice(index, 1);
        }   
       
      }
    });
  }

  RemoveFromBag(item) {
    this.addtobagitems.uid = Number.parseInt(sessionStorage.getItem("uid"));
    this.addtobagitems.itm_id = item["itm_id"];
    this.addtobagitems.bag_id = item["bag_id"];
    this.addtobagitems.price = item["price"];
    this.addtobagitems.qunty = item["qunty"];
    this.addtobagitems.type = 2; //for remove

    this.dataservice.Comm_Service_Movetowhishlist_Remove(this.addtobagitems).subscribe(x => {
      //   console.log(x)

      if (x["res"] = "Successfull") {
        this.toastr.info("Removed Item From Bag", "BAG", { timeOut: 3000, positionClass: 'toast-top-right' });
        const index: number = this.BagItems.indexOf(item);
        if (index !== -1) {
          this.BagItems.splice(index, 1);
        }

      }
    });
  }


  checkout() {
    this.router.navigate(['/checkout']);
}
}
