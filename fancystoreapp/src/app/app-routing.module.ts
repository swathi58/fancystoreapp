import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WomenstoreComponent } from './womenstore/womenstore.component';
import { HomeComponent } from './home/home.component';
import { KurtisComponent } from './womenstore/kurtis/kurtis.component';
import { SidemenuComponent } from './womenstore/sidemenu/sidemenu.component';
import { LoginComponent } from '../app/login/login.component';
import { UsersettingsComponent, orders, EditProfile } from '../app/usersettings/usersettings.component';
import { ListComponentComponent } from './list-component/list-component.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { VendorComponent } from './vendor/vendor.component';
import { LogoutComponent } from './logout/logout.component';
import { ItemDescpComponent } from './item-descp/item-descp.component';
import { AddToBagComponent } from './add-to-bag/add-to-bag.component';
import { WhishListComponent } from './whish-list/whish-list.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { CheckOutComponent } from './check-out/check-out.component';

const routes: Routes = [
  {

    path: 'womenstore',
    component: WomenstoreComponent
  },
  {

    path: 'home',
    component: HomeComponent
  }, 
  {
    path: 'kurtis',
    component: KurtisComponent
  },
  {
    path: "sidemenu",
    component: SidemenuComponent
  },
  {
    path: 'usersettings',
    component:UsersettingsComponent
  },
  {
    path: 'usersettings/orders',
    component: orders
  },
  {
    path: 'usersettings/editprofile',
    component: EditProfile
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'itemdesc',
    component: ItemDescpComponent
  },

  //10/12/2018
  {
    path: 'itembag',
    component: AddToBagComponent
  },


  {
    path: 'whishlist',
    component: WhishListComponent
  },

  //17/12/2018

  {

    path: 'checkout',
    component: CheckOutComponent
  },



 //sudha
 {
  path: 'vendor',
  component: VendorComponent

},
{
  path: 'Eprofile/:Role', 
  component: EditProfileComponent    
},
{
  path: 'welcome',
  component: WelcomeComponent,
  pathMatch: 'full'
},
{
  path:'List/:Role/:LType',
  component: ListComponentComponent
},

  { path:'vendor',
    component:VendorComponent
  },
  {
    path:'welcome',
    component:WelcomeComponent
  }
  //{
  //  path: '', 
  //  component: WelcomeComponent, 
  //  pathMatch: 'full'
  //},
  //{
  //  path: 'welcome',
  //  component: WelcomeComponent,
  //  pathMatch: 'full'
  //},
  //{
  //  path:'List',
  //  component: ListComponentComponent
  //}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

 
}
