﻿using SMSFCart.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Text;
using System.Web;
using System.Net;



namespace SMSFCart.DAL
{


    public class VendorDb : DBConnection
    {
        DBConnection db = new DBConnection();
        SqlConnection ConCAP;
        SqlCommand CmdCAP;
        //SqlDataAdapter DaCAP;
        DataSet DsCAP;
        DataTable dt;
        string ErrorMessage;
        SqlTransaction tranCAP;
        User objuser = new User();


        public int Vendorregister(User user)
        {
            string str = db.Sconnection();
            string ImageExt = "";
            string logoext = "";
            if (user.Image != null || user.Image != "")
            {
                string fname = user.Image;
                ImageExt = Path.GetExtension(fname);
            }
            if (user.Logo != null || user.Logo != "")
            {
                string logo = user.Logo;
                logoext = Path.GetExtension(logo);
            }





            int vid = 0;
            CmdCAP = new SqlCommand();

            //CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Imode", SqlDbType.VarChar, 100, "Insupd"));
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Firstname", SqlDbType.VarChar, 100, string.IsNullOrEmpty(user.FirstName) ? SqlString.Null : user.FirstName));
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@LastName", SqlDbType.VarChar, 100, string.IsNullOrEmpty(user.LastName) ? SqlString.Null : user.LastName));
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Email", SqlDbType.VarChar, 100, string.IsNullOrEmpty(user.Email) ? SqlString.Null : user.Email));
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@PhoneNo", SqlDbType.VarChar, 100, string.IsNullOrEmpty(user.PhoneNo) ? SqlString.Null : user.PhoneNo));
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Gender", SqlDbType.VarChar, 10, string.IsNullOrEmpty(user.Gender) ? SqlString.Null : user.Gender));

            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Password", SqlDbType.VarChar, 100, string.IsNullOrEmpty(user.Password) ? SqlString.Null : user.Password));

            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Image", SqlDbType.VarChar, 100, ImageExt));
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@RoleID", SqlDbType.Int, 0, user.RoleID));

            //CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@PID", SqlDbType.Int, 0, user.PID));
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Logo", SqlDbType.VarChar, 100, logoext));

            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@IsActive", SqlDbType.Int, 0, user.IsActive));


            CmdCAP.CommandText = "Sp_vendorregistration";
            CmdCAP.CommandType = CommandType.StoredProcedure;

            using (ConCAP = new SqlConnection(str))
            {
                try
                {
                    ConCAP.Open();
                    tranCAP = ConCAP.BeginTransaction();
                    CmdCAP.Connection = ConCAP;
                    CmdCAP.Transaction = tranCAP;
                    CmdCAP.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    //CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@id", SqlDbType.Int, 0, ParameterDirection.Output));
                    int i = CmdCAP.ExecuteNonQuery();

                    tranCAP.Commit();

                    vid = Convert.ToInt32(CmdCAP.Parameters["@id"].Value);


                }
                catch (Exception ex)
                {
                    ErrorMessage = ex.Message;
                    if (tranCAP != null)
                        tranCAP.Rollback();
                }
            }

            return vid;
        }

        public DataTable bindproduct()
        {
            CmdCAP = new SqlCommand();
            //CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Imode", SqlDbType.VarChar, 100, "select"));
            CmdCAP.CommandText = "sp_producttypes";
            CmdCAP.CommandType = CommandType.StoredProcedure;
            dt = CommonDbFunctions.SetToDataTable(CmdCAP);
            return dt;
        }


        public DataSet GetAllVendors()
        {
            CmdCAP = new SqlCommand();

            DsCAP = CommonDbFunctions.FillDataSet("spGetAllVendorsList", CmdCAP);

            return DsCAP;
        }

        public DataTable bindpcategories(int pid)
        {
            CmdCAP = new SqlCommand();
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@pid", SqlDbType.Int, 0, pid));
            CmdCAP.CommandText = "sp_Products_Category";
            CmdCAP.CommandType = CommandType.StoredProcedure;
            dt = CommonDbFunctions.SetToDataTable(CmdCAP);
            return dt;
        }

        public DataTable bindpscategories(int prod_cat_id)
        {
            CmdCAP = new SqlCommand();
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@prod_cat_id", SqlDbType.Int, 0, prod_cat_id));
            CmdCAP.CommandText = "sp_Products_Subcategory";
            CmdCAP.CommandType = CommandType.StoredProcedure;
            dt = CommonDbFunctions.SetToDataTable(CmdCAP);
            return dt;
        }

        public string inspname(string pname)
        {
            string str = db.Sconnection();
            CmdCAP = new SqlCommand();

            //CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@Imode", SqlDbType.VarChar, 50, "Insproud"));
            CmdCAP.Parameters.Add(CommonDbFunctions.AddParameter("@pname", SqlDbType.VarChar, 50, string.IsNullOrEmpty(pname) ? SqlString.Null : pname));
            CmdCAP.CommandText = "Sp_vendor";
            CmdCAP.CommandType = CommandType.StoredProcedure;

            using (ConCAP = new SqlConnection())
            {
                try
                {
                    ErrorMessage = CommonDbFunctions.SqlCommandExecution(CommandType.StoredProcedure, "Sp_vendor", CmdCAP);

                }
                catch (SqlException ex)
                {
                    ErrorMessage = ex.Message;
                }
            }
            return ErrorMessage;

        }
    }
}
