﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMSFCart.Models
{
    public class RandomNamesForProfiles
    {
        public static string GetRandomString(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[passwordLength];
            Random random = new Random();

            bool containsNum = false;
            do
            {
                for (int i = 0; i < passwordLength; i++)
                {
                    chars[i] = allowedChars[random.Next(0, allowedChars.Length)];
                    if (Char.IsDigit(chars[i]))
                    {
                        containsNum = true;
                    }
                }
            } while (!containsNum);

            return new string(chars);
        }

    }
}
