﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using SMSFCart.DAL;
using SMSFCart.BOL;
using System.IO;
//using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMSFCart.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorController : ControllerBase
    {
        public IConfiguration IConfiguration { get; }
        private IHostingEnvironment _hostingEnvironment;

        public VendorController(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        //EntryBs objEntry = new EntryBs();
        User vendor = new User();
        IActionResult IARresponse;
        VendorDb objVendorDb = new VendorDb();
        DataSet ds = new DataSet();
        //string Ext = "";
        string fname = "";
        DataTable TableData = new DataTable();
        [HttpPost]
        [Route("Register")]
        public IActionResult Register([FromBody] User model)
        {

            IARresponse = BadRequest();
            try
            {
                vendor.FirstName = model.FirstName;
                vendor.LastName = model.LastName;
                vendor.Email = model.Email;
                vendor.PhoneNo = model.PhoneNo;
                vendor.Gender = model.Gender;
                vendor.Password = model.Password;
                vendor.OTP = 0;//RandomNumForOTP.GetRandomString(6);
                if (model.Image != "")
                    vendor.Image = model.Image;
                vendor.RoleID = 2;
                //user.DOR = model.DOR;
                vendor.VID = model.VID;
                //user.PID = model.PID;

                if (model.IsActive != 0)
                    vendor.IsActive = model.IsActive;
                else
                    vendor.IsActive = 0;

                if (model.Logo != "")
                    vendor.Logo = model.Logo;
                int vid = objVendorDb.Vendorregister(vendor);



                if (vid != 0)
                {

                    IARresponse = Ok(new { res = "Vendor Registed Successfully" });

                    if (model.Image != "")
                    {
                        string root = _hostingEnvironment.WebRootPath + "/vendor/pictures";
                        var fname = Path.GetFileName(vendor.Image);
                        var path = Path.Combine(root, fname);
                        //string[] arrfiles = Directory.GetFiles(HttpContext.Current.Server.MapPath("~/vendor/"));
                        string[] arrfiles = Directory.GetFiles(root);

                        //var files = HttpContext.Request.Form.Files;
                        string fileExt = Path.GetExtension(fname);

                        if (fname != null && fname.Length > 0)
                        {
                            var file = fname + "_" + vid + fileExt;

                            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "vendor");
                            if (file.Length > 0)
                            {
                                //var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file);
                                var fileName = Path.GetExtension(file);
                                using (var fileStream = new FileStream(Path.Combine(root, file), FileMode.Create))
                                {
                                    //await file.CopyToAsync(fileStream);
                                    //employee.ImageName = fileName;
                                    vendor.Image = fileName;
                                }

                            }
                        }

                        foreach (string filename in arrfiles)
                        {
                            if (System.IO.File.Exists(filename))
                            {


                                if (fname == Path.GetFileName(filename))
                                {
                                    string fileNamenext = Path.GetFileNameWithoutExtension(filename);


                                    // FileInfo fffinfo = new FileInfo(Path.Combine(fileNamenext+vid+fileExt));

                                    FileInfo finfo = new FileInfo(filename);
                                    finfo.CopyTo(path + "_" + vid + fileExt, true);
                                    System.IO.File.Delete(filename);
                                }



                            }

                        }
                    }


                    if (model.Logo != "")
                    {
                        string lroot = _hostingEnvironment.WebRootPath + "/vendor/logo";
                        var lfname = Path.GetFileName(vendor.Logo);
                        var path = Path.Combine(lroot, lfname);
                        //string[] arrfiles = Directory.GetFiles(HttpContext.Current.Server.MapPath("~/vendor/"));
                        string[] arrfiles = Directory.GetFiles(lroot);

                        //var files = HttpContext.Request.Form.Files;
                        string lfileExt = Path.GetExtension(lfname);

                        if (lfname != null && lfname.Length > 0)
                        {
                            var lfile = lfname + "_" + vid + lfileExt;

                            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "vendor/logo");
                            if (lfile.Length > 0)
                            {
                                //var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file);
                                var lfileName = Path.GetExtension(lfile);
                                using (var fileStream = new FileStream(Path.Combine(lroot, lfile), FileMode.Create))
                                {
                                    //await file.CopyToAsync(fileStream);
                                    //employee.ImageName = fileName;
                                    vendor.Logo = lfileName;
                                }

                            }
                        }

                        foreach (string lfilename in arrfiles)
                        {
                            if (System.IO.File.Exists(lfilename))
                            {


                                if (fname == Path.GetFileName(lfilename))
                                {
                                    string fileNamenext = Path.GetFileNameWithoutExtension(lfilename);


                                    // FileInfo fffinfo = new FileInfo(Path.Combine(fileNamenext+vid+fileExt));

                                    FileInfo finfo = new FileInfo(lfilename);
                                    finfo.CopyTo(path + "_" + vid + lfileExt, true);
                                    System.IO.File.Delete(lfilename);
                                }



                            }

                        }
                    }
                }



                else
                {
                    IARresponse = BadRequest(new { error = "Vendor Not registred... Try Again Later!.." + vid });
                }

            }
            catch (Exception ex)
            {
                IARresponse = BadRequest(new { error = ex.Message });



            }
            return IARresponse;
        }

        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }

        //[HttpGet, Route("getproduct")]
        //public User[] getproduct()
        //{
        //    var detials = new List<User>();
        //    //TableData.Clear();
        //    TableData = objVendorDb.bindproduct();
        //    detials.AddRange(from DataRow dt in TableData.Rows
        //                     select new User
        //                     {
        //                         PName = dt["PName"].ToString(),
        //                         PID = Convert.ToInt32(dt["PID"].ToString()),
        //                     });
        //    return detials.ToArray();
        //}

        //[HttpPost,Route("insproductname/{pName}")]
        //public IActionResult insproductname(string pName)
        //{
        //    IARresponse = BadRequest();
        //    string msg;
        //     msg=objVendorDb.inspname(pName);
        //    if (msg == "")

        //        IARresponse = Ok(new { res = "product name added successfully" });

        //    else
        //        IARresponse = BadRequest(new { err = "Try..again" }); 
        //    return IARresponse;

        //}

      
       

        [HttpGet, Route("getpscategories/{prod_cat_id}")]
        public User[] getpscategories(int prod_cat_id)
        {
            var detials = new List<User>();
            //TableData.Clear();
            if (prod_cat_id != 0)
            {
                TableData = objVendorDb.bindpscategories(prod_cat_id);
                detials.AddRange(from DataRow dt in TableData.Rows
                                 select new User
                                 {
                                     pscname = dt["pscname"].ToString(),
                                     prod_subcat_id = Convert.ToInt32(dt["prod_subcat_id"].ToString()),
                                 });
            }
            return detials.ToArray();
        }

        [HttpPost, Route("insproductname/{pName}")]
        public IActionResult insproductname(string pName)
        {
            IARresponse = BadRequest();
            string msg;
            msg = objVendorDb.inspname(pName);
            if (msg == "")

                IARresponse = Ok(new { res = "product name added successfully" });

            else
                IARresponse = BadRequest(new { err = "Try..again" });
            return IARresponse;

        }

        //sudha on 14/12
        [HttpGet]
        [Route("GetAllVendorsList")]
        public IActionResult GetAllVendorsList()
        {
            IARresponse = Unauthorized();
            string path = "";

            try
            {
                DataSet vds = new DataSet();

                vds = objVendorDb.GetAllVendors();

                if (vds != null)
                {

                    for (int iDSCount = 0; iDSCount < vds.Tables[0].Rows.Count; iDSCount++)
                    {
                        //Console.WriteLine(vds.Tables[0].Rows[iDSCount]["Image"].ToString());
                        if (vds.Tables[0].Rows[iDSCount]["Image"].ToString() != null)
                        {
                            path = _hostingEnvironment.WebRootPath + "/images/ProfilePhotos/" + vds.Tables[0].Rows[iDSCount]["Image"].ToString();
                            try
                            {
                                if (System.IO.File.Exists(path))
                                {
                                    byte[] b = System.IO.File.ReadAllBytes(path);
                                    vds.Tables[0].Rows[iDSCount]["Image"] = "data:image/png;base64," + Convert.ToBase64String(b);
                                }
                                else
                                {
                                    vds.Tables[0].Rows[iDSCount]["Image"] = null;
                                }
                            }
                            catch (FileNotFoundException ex)
                            {
                                vds.Tables[0].Rows[iDSCount]["Image"] = null;
                                Console.WriteLine(ex.Message + "  in VendorController GetAllVendors()");
                            }

                        }
                    }
                
         
        //if (vds.Image != "")
        //{
        //    path = _hostingEnvironment.WebRootPath + "/images/ProfilePhotos/" + user.Image;

        //    try
        //    {
        //        if (System.IO.File.Exists(path))
        //        {
        //            byte[] b = System.IO.File.ReadAllBytes(path);
        //            user.Image = "data:image/png;base64," + Convert.ToBase64String(b);
        //        }
        //        else
        //        {
        //            user.Image = null;
        //        }

        //    }
        //    catch (FileNotFoundException ex)
        //    {
        //        user.Image = null;
        //        Console.WriteLine(ex.Message + "  in UserController Get()");
        //    }

        IARresponse = Ok(new
                    {
                        vendordata = vds
                    });
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message + "  in VendorController GetAllVendorsList()");

            }
            return IARresponse;

        }

    }
}
