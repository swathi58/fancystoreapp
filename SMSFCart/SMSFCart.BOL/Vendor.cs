﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SMSFCart.BOL
{
    public partial class Vendor
    {
        public string Logo { get; set; }
        public string PID { get; set; }
        public string DOR { get; set; }
        public string IsActive { get; set; }
    }
}
